<?php
get_header();
$postid = get_the_ID();
?>
<section id="topo" style="   background: url('<?php print get_template_directory_uri();?>/assets/image/bg-topo.jpg');">
    <div class="pattern azul"></div>
    <div class="container">
        <div class="row">
            <div class="box-topo">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                    <h1 class=" text-uppercase">
                        <?php the_title(); ?>
                    </h1>

                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php print site_url();?>" >Home</a>
                        </li>
                        <li class="active">
                            <?php print $page_title = $wp_query->post->post_title;?>
                        </li>
                    </ol>

                    <div class="descricao">
                        <h4>
                            <?php print get_field('servico_subtitulo',$postid); ?>
                    </div>
                    <a class="btn btn-cliente" href="#contato" alt="<?php print ucwords(the_title());?> - Agência Digital Ópera" title="<?php print ucwords(the_title());?> - Agência Digital Ópera" title="">QUERO ESCUTAR VOCÊS</a>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                     <img class="img-responsive" src="<?php print get_field('servico_imagem',$postid); ?>" alt="" >
                </div>
            </div>
        </div>
</section>
<section id="barra1">
    <div class="bigtitulo hidden-xs hidden-sm">
        <h2 class="wow bounceInRight" data-wow-delay="0.3s">
            <?php print $page_title = $wp_query->post->post_title;?>
        </h2>
    </div>
    <div class="container">
        <div class="row-eq-hight display-table">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="imagem">
                    <img class="img-responsive" src="<?php print get_field('servico_icone_negativo',$postid); ?>" alt="">
                </div>
                <!-- /.imagem -->
            </div>
            <!-- /.col-xs-12 col-sm-12 col-md-4 col-lg-4"> -->
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style=" ">
                <div class="descricao">
                    <?php print get_field('servico_sessao_verde',$postid); ?>
                </div>
                <!-- /.descricao -->
            </div>
            <!-- /.col-xs-12 col-sm-12 col-md-8 col-lg-8 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /#barra1 -->
<section id="barra2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="descricao">
                    <?php print get_field('servico_sessao_branca',$postid); ?>
                </div>
            </div>
            <!-- /.col-xs-12 col-sm-12 col-md-12 col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /#barra2 -->
<hr>
<section id="barra3"  class="">
    <div class="container">
        <div class="row">

            <div class="titulo-principal">
                <h2>
                    Outros Serviços
                </h2>
            </div>
            <div class="servicos">
                <?php
                $args = array(
                    'post_type' => 'servicos',
                    'orderby'     => 'rand',
                    'posts_per_page' =>5,
                    'post__not_in' => array($postid)
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) :
                    $i = 1;
                    while ( $query->have_posts() ) : $query->the_post();
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-2 <?php if($i == 1){print 'col-md-offset-1';}?>">
                            <div class="imagem">
                                <a href="" class="" alt="Criação de Sites - Agência Digital Ópera" title="Criação de Sites - Agência Digital Ópera">
                                    <img src="<?php print get_field('servico_icone_positivo');?>" alt="">
                                </a>
                                <div class="titulo text-center">
                                    <a href="" class="" alt="Criação de Sites - Agência Digital Ópera" title="Criação de Sites - Agência Digital Ópera">
                                        <h3>
                                            <?php print the_field('titulo_abreviado'); ?>
                                        </h3>
                                    </a>
                                </div>
                                <a href="<?php print get_permalink($post->ID);?>" class="botao btn btn-cliente" alt="Criação de Sites - Agência Digital Ópera" title="Criação de Sites - Agência Digital Ópera">Clique aqui</a>
                                <!-- /.botao btn btn-cliente -->
                                <!-- /.descricao -->
                                <!-- /.titulo -->
                            </div>
                            <!-- /.imagem -->
                        </div>
                        <!-- /.col-xs-12 col-sm-12 col-md-3 col-lg-3 -->
                <?php
                $i++;

                    endwhile;
                endif;
                ?>

            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /#barra3 -->
<section id="contato">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="titulo">
                    <h2>Contato</h2>
                </div>
                <!-- /.titulo -->
                <ul>
                    <li class="contato"><p>Contato@agenciaopera.com.br</p></li>
                    <!-- /.contato -->
                    <li class="contato"><p><a href="tel:8130348811">(81) 3034-8811</a></p></li>
                    <!-- /.contato -->
                    <li class="endereco"><p>Rua Barão de São Borja, 62</p></li>
                    <!-- /.endereco -->
                    <li class="endereco"><p>Edf. Sigma Center, Sala 201</p></li>
                    <!-- /.endereco -->
                    <li class="endereco"><p>Boa Vista, Recife - PE</p></li>
                    <!-- /.endereco -->
                </ul>
                <ul class="list-inline">
                    <li class="redessociais"><a href="http://www.facebook.com.br/agenciaopera" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <!-- /.redessociais -->
                    <!--li class="redessociais"><a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li-->
                    <!-- /.redessociais -->
                    <li class="redessociais"><a href="http://www.instagram.com/agenciaopera" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <!-- /.redessociais -->
                    <li class="redessociais"><a href="https://www.linkedin.com/company-beta/3242764/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <!-- /.redessociais -->
                </ul>
                <!-- /.list-inline -->
            </div>
            <!-- /.col-xs-12 col-sm-12 col-md-6 col-lg-6 -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                <form id="formContato" enctype="multipart/form-data">
                    <input type="text" name="nome" placeholder="Nome" id="" required>
                    <input type="text" name="email" placeholder="Email" id="" required>
                    <input type="text" name="telefone" placeholder="Telefone" id="" required>
                    <!--label for="curriculo" class="anexar_curriculo btn btn-cliente btn-block btn-outlined">Anexar Currículo</label>
                    <input type="file" name="curriculo" id="curriculo" accept=".pdf, .doc, .docx"  style="display: none">
                    <input type="text" id="curriculo_file" style="display: none" readonly="readonly"-->
                    <textarea type="text" rows="5"  name="mensagem" placeholder="Digite sua mensagem" id="" required></textarea>
                    <div class="g-recaptcha" data-sitekey="6Ldvoh0UAAAAANYzVGL1URdJbRXbEQSq1ZLau0_H" data-callback="enableSend"></div>
                    <button type="submit" id="submitContato" class="btn btn-cliente2"> Fale com a Ópera  <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></button>
                </form>
            </div>
            <!-- /.col-xs-12 col-sm-12 col-md-6 col-lg-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<?php get_footer(); ?>
