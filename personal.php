<?php include('header.php'); 

$background_topo = "x";
?>
<section id="topo" style=" background-color:#fff !important;  background: url('<?php //print get_template_directory_uri();?> assets/imagens/group-training.jpg');">
    <div class="pattern azul"></div>
    <div class="container">
        <div class="row">
            <div class="box-topo">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <h1 class=" text-uppercase text-center">
                        <?php// the_title(); ?>
                        Personal
                    </h1>

                    <ul class="breadcrumb text-center">
                    	<li class="text-uppercase">
                            <a href="<?php //print site_url();?>" >Home</a>
                        </li>
                        <li class="active text-uppercase">
                            <?php // print $page_title = $wp_query->post->post_title;?>
                            Personal
                        </li>
                    </li>

                    <div class="descricao">
                        <h4>
                            <?php //print get_field('servico_subtitulo',$postid); ?>
                        </h4>
                    </div>
                  
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                     <img class="img-responsive" src="<?php// print// get_field('servico_imagem',$postid); ?>" alt="" >
                </div>
            </div>
        </div>
</section>

<section id="personal" class="">
    <div class="container">
        <div class="row">
        	 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="imagem">
                    <img  class="img-responsive" src="assets/imagens/profile.png" alt="">
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="descricao">
                    <p>
                      Estar em movimento. Mesmo de forma inconsciente, isso é o que sempre me
						motivou. Ainda muito novo, por volta dos meus 4 ou 5 anos, eu já praticava esportes
						de forma regular e era vidrado nessas atividades. Comecei com Natação, passei pelo
						Judô e aos 8 anos já era jogador oficial no time de Futebol da escola e considerado
						um craque.
					</p>
					<p>
						Minhas habilidades com a bola me renderam a oportunidade de jogar futebol pela
						seleção sub 15 de El Salvador, país em que morei por dois anos durante minha pré-
						adolescência. Não segui a carreira no futebol, por uma decisão do meu pai, e com 14
						anos voltei a morar no Brasil, onde troquei o esporte pelas artes marciais e
						musculação.
					</p>
					<p>
						E foi com a musculação que acabei me apaixonando pelos treinamentos físicos e
						tornando essa paixão minha profissão. Formado em Educação Física pela UNIBRA-
						PE, e com diversas especializações voltadas para preparo e tratamento físico, atuo na
						área há 8 anos, levando uma qualidade de vida e saúde melhor para as pessoas.
						Já passei por diversas academias do Recife, fui líder e responsável técnico por dois
						anos em uma academia da Zona Sul da cidade e atualmente trabalho com atletas de
						alto rendimento do futebol de areia e jiu-jistu, sou personal trainer com aulas
						presenciais e estou embarcando nesse novo projeto incrível que o Follow UP!
					</p>
					
						<strong>	
							Conheça mais sobre as minhas qualificações:
						</strong>
						<ul>
							<li>
								Licenciado e bacharel em Educação Física | UNIBRA-PE
							</li>
							<li>
								Extensão em Avaliação Física voltada para a saúde | UNIBRA-PE
							</li>
							<li>
								Especialização em coordenação e gestão fitness | IHRSA-SP
							</li>
							<li>
								Desenvolvimento e Liderança em Programação NeuroLinguística (PNL) | INEXH-PE
							</li>
							<li>
								Terapia Artro Miofascial por bandagens ( Kinesio Tape ) | AMF-PE
							</li>
							<li>
								Especialização em bases fisiológicas do treinamento personalizado e nutrição
							esportiva | Instituto Valoriza-ES
							</li>
							<li>
								Especialização em Quiropraxia Manual | Instituto Terapia Manual | SP (em
							andamento)
							</li>
						</ul>
						
                    
                </div>
            </div>
        </div>
    </div>
</section>
<section id="barra-planos"  style="   background: url('<?php //print get_template_directory_uri();?> assets/imagens/group-training.jpg');">
	<div class="pattern azul"></div>
	<div class="container-fluid">
		<div class="row">
			<div class="titulo text-center">
				<h1>
				CONHEÇA OS PLANOS DISPONÍVEIS
				</h1>
			</div>
		</div>
		<a href="planos.php" class="btn btn-cliente-amarelo"> TREINE COMIGO </a>
	</div>
</section>
<?php include('footer.php'); ?>
