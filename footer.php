<?php
/**
 * Created by PhpStorm.
 * User: João
 * Date: 25/06/2018
 * Time: 23:08
 */
?>
<section id="footer" class="nopadding">
    <div class="container">
        <div class="row">
            <div class="copyright">
                <p>
                    Desenvolvido por <a href="#">AC Agência Digital</a>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<script src="assets/js/script.js"></script>
<!-- Bootstrap JavaScript -->
<script src="assets/utils/bootstrap/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->


<script>
	$(document).ready(function () {
	 $('nav a[href*="#"]:not([href="#"])').on('click',function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top-90
                    }, 1000);
                    return false;
                }
            }
        });
	});

</script>
</body>
</html>
