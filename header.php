<?php
/**
 * Created by PhpStorm.
 * User: João
 * Date: 25/06/2018
 * Time: 23:08
 */
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title Page</title>


    <!-- Fontawesomw CSS -->
    <link rel="stylesheet" href="assets/utils/fontawesome/css/all.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/utils/bootstrap/css/bootstrap.min.css">
    <!-- Estilo CSS -->
    <link rel="stylesheet" href="assets/estilos/css/estilo.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="loading">
    <div class="logo">
        <img src="assets/imagens/logo.png" alt="" class="img-responsive">
    </div>
</div>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">

    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">
                <div class="logo">
                    <img src="assets/imagens/logo.png" alt="" class="img-responsive">
                </div>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">

            <ul class="nav navbar-nav navbar-right">

                <li><a href="#Home">Home</a></li>
                <li><a href="#personal">Personal</a></li>
                <li><a href="#">Tática</a></li>
                <li><a href="planos.php">Planos</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#contato">Contato</a></li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </div>

</nav>