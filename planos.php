<?php include('header.php'); ?>

<section id="topo" style=" background-color:#fff !important;  background: url('<?php //print get_template_directory_uri();?> assets/imagens/group-training.jpg');">
    <div class="pattern azul"></div>
    <div class="container">
        <div class="row">
            <div class="box-topo">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <h1 class=" text-uppercase text-center">
                        <?php// the_title(); ?>
                        Planos
                    </h1>

                    <ul class="breadcrumb text-center">
                    	<li class="text-uppercase">
                            <a href="<?php //print site_url();?>" >Home</a>
                        </li>
                        <li class="active text-uppercase">
                            <?php // print $page_title = $wp_query->post->post_title;?>
                            Planos
                        </li>
                    </li>

                    <div class="descricao">
                        <h4>
                            <?php //print get_field('servico_subtitulo',$postid); ?>
                        </h4>
                    </div>
                  
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                     <img class="img-responsive" src="<?php// print// get_field('servico_imagem',$postid); ?>" alt="" >
                </div>
            </div>
        </div>
</section>
<section id="planos">
	<div class="container">
		<div class="row box-content">
			<div class="col-md-12">
				<div class="titulo text-center">
					<h2>
						Confira os pacotes para o Recife:
					</h2>
				</div>
				<div class="subtitulo">
					<p class="text-center">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus aspernatur consequatur, dolores doloremque laboriosam nulla tempora culpa repellat esse quo labore rerum hic perspiciatis soluta modi inventore aliquam, dolorum recusandae!
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container content">
	
		<div class="row">
			<div class="format-flex">
				
				<div class="col-md-4">
					<div class="pricing hover-effect">
						<div class="pricing-head">
							<h3>
								PACOTE 3 
								<span>
									100% ONLINE 
								</span>
							</h3>
							<h4>
								
								<i>R$</i>200<i>.00</i>
								
							</h4>
						</div>
						<ul class="pricing-content list-unstyled">
							<li>
								Tempo: 3 meses
							</li>
							<li>
								Acompanhamento: Não presencial (Online)
							</li>
							<li>
								Avaliação ínicial e Final
							</li>
							<li>
								Formas de Pagamento: Até 3x no cartão de Crédito
							</li>
						
						</ul>
						<div class="pricing-footer">
							<p>
								 <br>
							</p>
							<a href="javascript:;" class="btn btn-cliente-amarelo">
								Contratar
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="pricing pricing-active hover-effect">

						<div class="pricing-head pricing-head-active">
							<h3>
								PACOTE 9
								<span>
									6 Aulas presenciais 
								</span>
							</h3>
							<h4>
								<i>R$</i>8<i>.69</i>
								<span>
									PLANO MAIS COMPRADO!
								</span>
							</h4>
						</div>
						<ul class="pricing-content list-unstyled">
							<li>
								Tempo: 9 meses
							</li>
							<li>
								Acompanhamento: 6 Aulas presenciais
							</li>
							<li>
								Avaliação ínicial e Final (Online)
								<br>
								Avaliação física presencial trimestral
							</li>
							<li>
								Formas de Pagamento: Até 9x no cartão de Crédito
							</li>
						
						</ul>
						<div class="pricing-footer">
							<p>
								<strong>
								 Desconto de 7% na renovação semestral do pacote
								</strong>
							</p>
							<a href="javascript:;" class="btn btn-cliente-amarelo">
							Contratar
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="pricing hover-effect">
						<div class="pricing-head">
							<h3>
								PACOTE 6
								<span>
								3 Aulas Presenciais
								</span>
							</h3>
							<h4><i>R$</i>350<i>.00</i>
						</div>
						<ul class="pricing-content list-unstyled">
							<li>
								Tempo: 6 meses
							</li>
							<li>
								Acompanhamento: 3 Aulas presenciais
							</li>
							<li>
								Avaliação ínicial e Final
							</li>
							<li>
								Formas de Pagamento: Até 6x no cartão de Crédito
							</li>
						
						</ul>
						<div class="pricing-footer">
							<p>
								<strong>
								 Desconto de 5% na renovação semestral do pacote
								</strong>
							</p>
							<a href="javascript:;" class="btn btn-cliente-amarelo">
							Contratar
							</a>
						</div>
					</div>
				</div>
				
				
			</div>
			<!-- Pricing -->

			<!--//End Pricing -->
		</div>
	</div>
	<div class="container">
		<div class="row">
			<hr>	
		</div>
	</div>

	<div class="container">
		<div class="row box-content">
			<div class="col-md-12">
				<div class="titulo text-center">
					<h2>
						Confira os pacotes nacionais e internacionais:
					</h2>
				</div>
				<div class="subtitulo">
					<p class="text-center">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus aspernatur consequatur, dolores doloremque laboriosam nulla tempora culpa repellat esse quo labore rerum hic perspiciatis soluta modi inventore aliquam, dolorum recusandae!
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container content">
	
		<div class="row">
			<div class="format-flex">
				
				<div class="col-md-4">
					<div class="pricing hover-effect">
						<div class="pricing-head">
							<h3>
								PACOTE 3 
								<span>
									100% ONLINE 
								</span>
							</h3>
							<h4>
								
								<i>R$</i>200<i>.00</i>
								
							</h4>
						</div>
						<ul class="pricing-content list-unstyled">
							<li>
								Tempo: 3 meses
							</li>
							<li>
								Acompanhamento: Online
							</li>
							<li>
								Avaliação Física ínicial e Final
							</li>
							<li>
								Formas de Pagamento: Até 3x no cartão de Crédito
							</li>
						
						</ul>
						<div class="pricing-footer">
							<p>
								 <strong>
								 *Desconto de 5% na compra + Vale presente
								 <br> 10% para presentear amigo na
									compra de qualquer pacote Follow Up
								 </strong>
							</p>
							<a href="javascript:;" class="btn btn-cliente-amarelo">
								Contratar
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="pricing pricing-active hover-effect">

						<div class="pricing-head pricing-head-active">
							<h3>
								PACOTE 9 
								<span>
								6 Aulas por Vídeo Conferência
								</span>
							</h3>
							<h4><i>R$</i>550<i>.00</i>
								<span>
								PLANO MAIS COMPRADO!
								</span>
							</h4>
						</div>
						<ul class="pricing-content list-unstyled">
							<li>
								Tempo: 9 meses
							</li>
							<li>
								Acompanhamento: 6 Aulas presenciais
							</li>
							<li>
								Avaliação ínicial e Final (Online)
							</li>
							<li>
								Formas de Pagamento: Até 9x no cartão de Crédito
							</li>
						
						</ul>
						<div class="pricing-footer">
							<p>
								<strong>
								 Desconto de 9% na renovação semestral do pacote
								</strong>
							</p>
							<a href="javascript:;" class="btn btn-cliente-amarelo">
							Contratar
							</a>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="pricing hover-effect">
						<div class="pricing-head">
							<h3>
								PACOTE 6
								<span>
								6 Aulas por Vídeo Conferência
								</span>
							</h3>
							<h4><i>R$</i>350<i>.00</i>
						</div>
						<ul class="pricing-content list-unstyled">
							<li>
								Tempo: 6 meses
							</li>
							<li>
								Acompanhamento: 6 Aulas por Vídeo Conferência para Dúvidas
							</li>
							<li>
								Avaliação ínicial e Final (Online)
							</li>
							<li>
								Formas de Pagamento: Até 6x no cartão de Crédito
							</li>
						
						</ul>
						<div class="pricing-footer">
							<p>
								<strong>
								 Desconto de 7% na renovação semestral do pacote
								</strong>
							</p>
							<a href="javascript:;" class="btn btn-cliente-amarelo">
							Contratar
							</a>
						</div>
					</div>
				</div>
				
				
			</div>
			<!-- Pricing -->

			<!--//End Pricing -->
		</div>
	</div>

</section>

 <?php include('footer.php'); ?>
