<?php include('header.php'); ?>
<section id="carrousel" class="nopadding">
    <div class="container-fluid">
        <div class="row">

            <div id="carousel-id" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <img alt="" src="assets/imagens/banners/1.jpg">
                        <div class="container hidden">
                            <div class="carousel-caption">
                                <h1>Example headline.</h1>
                                <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
                                <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
    </div>
</section>
<section id="personal" class="nopadding">
    <div class="container">
        <div class="row">
            <div class="align-vertical">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                    <div class="titulo">
                        <i class="fas fa-dumbbell"></i>
                        <h1>
                              Personal
                        </h1>
                    </div>
                    <div class="subtitulo hidden">
                        <h3>

                        </h3>
                    </div>

                    <div class="descricao">
                        <p>
                           Estar em movimento. Mesmo de forma inconsciente, isso é o que sempre me motivou. Ainda muito novo, por volta dos meus 4 ou 5 anos, eu já praticava esportes
                            de forma regular e era vidrado nessas atividades. 
                        </p>
                        <p>
                        Comecei com Natação, passei pelo Judô e aos 8 anos já era jogador oficial no time de Futebol da escola e considerado um craque.
                        </p>
                    </div>
                    <div class="botao">
                        <a href="personal.php"  class="btn  btn-cliente-amarelo nodecoration">
                            Ler mais
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="imagem">
                        <img  class="img-responsive" src="assets/imagens/profile.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="folowup" class="nopaddingtop">
    <div class="container-fluid">
        <div class="row">
            <div class="barra">
                <div class="title text-center">
                    <h2>
                        FollowUP Físico
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="box-imagem">
                    <div class="imagem">
                        <img class="img-responsive" src="assets/imagens/folowup.png" alt="">
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="descricao" >
                    <h4 class="text-uppercase bold" style="font-weight: bold">O que é o FollowUp Físico: </h4>
                    <p>O Follow UP é um programa de acompanhamento online para treinamentos físicos personalizados, com embasamentos fisiológicos e que pode ser feito em qualquer lugar do mundo e na hora que você quiser.</p>
                    <p>Falta de tempo e espaço estão entre as maiores reclamações ou dificuldade que as pessoas têm na hora de começar a praticar um exercício e torná-lo um hábito. Mas com o Follow Up isso não é um problema.</p>
                    <p>Os treinamentos personalizados, ou seja, criados para atender as necessidades de cada aluno, são criados após uma análise completa das condições físicas e espaciais de cada pessoa. Assim posso fazer um treino que seja não só funcional, mas também divertido e prazeroso para você!</p>
                </div>
                <a href="pacotes.php" class="btn btn-cliente-azul">
                    Conhecer Planos disponíveis
                </a>
            </div>
        </div>
    </div>
</section>
<section id="depoimentos" class="nopadding">
    <div class="container-fluid">
        <div class="row">
            <div class="barra">
                <div class="title text-center">
                    <h2>
                        Depoimentos
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class='row'>
            <div class='col-md-offset-0 col-md-12'>
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Bottom Carousel Indicators -->


                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner">

                        <!-- Quote 1 -->
                        <div class="item active">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-circle" src="http://www.reactiongifs.com/r/overbite.gif" style="width: 100px;height:100px;">
                                        <!--<img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg" style="width: 100px;height:100px;">-->
                                    </div>
                                    <div class="col-sm-9">

                                        <p><i class="fas fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, delectus dolores doloribus ducimus ex ipsa minus nemo, numquam odio praesentium quos rem! Dolor dolore fugiat illum non sed sequi sint.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.</p>
                                        <small>Someone famous</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/mijustin/128.jpg" style="width: 100px;height:100px;">
                                    </div>
                                    <div class="col-sm-9">
                                        <p><i class="fas fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, delectus dolores doloribus ducimus ex ipsa minus nemo, numquam odio praesentium quos rem! Dolor dolore fugiat illum non sed sequi sint.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.</p>
                                        <small>Someone famous</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 3 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/keizgoesboom/128.jpg" style="width: 100px;height:100px;">
                                    </div>
                                    <div class="col-sm-9">
                                        <p><i class="fas fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, delectus dolores doloribus ducimus ex ipsa minus nemo, numquam odio praesentium quos rem! Dolor dolore fugiat illum non sed sequi sint.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.</p>
                                        <small>Someone famous</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>

                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="blog" class="nopadding">
    <div class="container-fluid">
        <div class="row">
            <div class="barra">
                <div class="title text-center">
                    <h2>
                        Blog
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="ultimos-posts-blog">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="imagem" style="width: 100%; height: 180px; background-size:cover; background-image: url('assets/imagens/post-blog.jpg'); background-position: center center;">
                        </div>
                        <div class="titulo">
                            <h4>
                                Título
                            </h4>
                        </div>
                        <div class="descricao">
                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis itaque porro quisquam sapiente similique tempora.</p>
                        </div>
                        <a href="#" class="btn btn-cliente-azul left">Ver Post</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="post">
                         <div class="imagem" style="width: 100%; height: 180px; background-size:cover; background-image: url('assets/imagens/post-blog.jpg'); background-position: center center;">
                        </div>

                        <!-- 
                        USAR COM EFEITO
                        <div class="imagem">
                            <img class="img-reponsive" src="http://via.placeholder.com/340x180" style="width: 100%">
                        </div> -->
                        <div class="titulo">
                            <h4>
                                Título
                            </h4>
                        </div>
                        <div class="descricao">
                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis itaque porro quisquam sapiente similique tempora.</p>
                        </div>
                        <a href="#" class="btn btn-cliente-azul left">Ver Post</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="post">
                         <div class="imagem" style="width: 100%; height: 180px; background-size:cover; background-image: url('assets/imagens/post-blog.jpg'); background-position: center center;">
                        </div>
                        <div class="titulo">
                            <h4>
                                Título
                            </h4>
                        </div>
                        <div class="descricao">
                            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis itaque porro quisquam sapiente similique tempora.</p>
                        </div>
                        <a href="#" class="btn btn-cliente-azul left">Ver Post</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="botao-ver-mais">
                        <a href="#" class="btn btn-cliente-amarelo text-center">
                            Ver mais postagens
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="redes-sociais" class="nopadding">
    <div class="container-fluid">
        <div class="row">
            <div class="barra">
                <div class="title text-center">
                    <h2>
                        Redes Sociais
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            	<div class="instagram" >
                    <div class="titulo">
                        <h3>
                            Instagram
                        </h3>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                            <img class="img-reponsive" src="http://via.placeholder.com/100x100">
                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>
</section>
<section id="contato">
    <div class="container-fluid">
        <div class="row">
            <div class="barra">
                <div class="title text-center">
                    <h2>
                       Contato
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-offset-2 col-md-8 col-lg-8">

                <form action="" method="POST" role="form">
                    <legend style="font-size:16px">
                        <p>
                            Preencha o formulário abaixo para tirar dúvidas e obter mais informações:
                        </p>
                    </legend>

                    <div class="form-group">
                        <label for="">Nome</label>
                        <input type="text" class="form-control" name="" required id="" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="">Telefone</label>
                        <input type="text" class="form-control" name="" required id="" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="">E-mail</label>
                        <input type="text" class="form-control" name="" required id="" placeholder="">
                    </div>

                    <div class="form-group">
                        <label for="">Mensagem</label>
                        <textarea class="form-control" name="" required id="" placeholder=""></textarea>
                    </div>






                    <button type="submit" class="btn btn-cliente-amarelo">Enviar</button>
                </form>
            </div>
        </div>
    </div>


</section>
<?php include('footer.php'); ?>